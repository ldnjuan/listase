/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Juan
 */
public class ListaDECircular {
    private NodoDE cabeza;

    public NodoDE getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoDE cabeza) {
        this.cabeza = cabeza;
    }

    @Override
    public String toString() {
        return "ListaCircular{" + "cabeza=" + cabeza + '}';
    }
    
    public int contarNodoDECircular() 
    {
        if(cabeza != null)
        {
           int cont = 0;
           NodoDE temp = cabeza;
           while(temp.getSiguiente() != cabeza)
           {
               cont++;
               temp = temp.getSiguiente();
           }
           return cont;
        } else {
            return 0;
        }
    }
    
    public void adicionarNodoDECircular(Infante dato, boolean esCabeza) 
    {
       if(cabeza != null)
       {
           NodoDE nuevo = new NodoDE(dato);
           nuevo.setSiguiente(cabeza);
           nuevo.setAnterior(cabeza.getAnterior());
           cabeza.getAnterior().setSiguiente(nuevo);
           cabeza.setAnterior(nuevo);
           
       } else {
           cabeza = new NodoDE(dato);
           cabeza.setSiguiente(cabeza);
           cabeza.setAnterior(cabeza);
       }
    }
    
    public NodoDE irAlUltimo()
    {
      if(cabeza != null)
      {
        return cabeza.getAnterior();
      }
        return null;
    }
}
