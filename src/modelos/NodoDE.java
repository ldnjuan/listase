/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Juan
 */
public class NodoDE {
    private Infante dato;
    private NodoDE siguiente;
    private NodoDE anterior;

    public NodoDE getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoDE anterior) {
        this.anterior = anterior;
    }

    public NodoDE(Infante dato) {
        this.dato = dato;
    }

    
    public Infante getDato() {
        return dato;
    }

    public void setDato(Infante dato) {
        this.dato = dato;
    }

    public NodoDE getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoDE siguiente) {
        this.siguiente = siguiente;
    }

    @Override
    public String toString() {
        return "NodoDE{" + "dato=" + dato + ", siguiente=" + siguiente + ", anterior=" + anterior + '}';
    }

    
    
    
    
}
