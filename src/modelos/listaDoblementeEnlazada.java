/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Juan
 */
public class listaDoblementeEnlazada {
    private NodoDE cabeza;

    public NodoDE getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoDE cabeza) {
        this.cabeza = cabeza;
    }

    @Override
    public String toString() {
        return "listaDoblementeEnlazada{" + "cabeza=" + cabeza + '}';
    }

    
    
    public void adicionarNodoDE(Infante dato) 
    {
       if(cabeza != null)
       {
           NodoDE temp = cabeza;
           while(temp.getSiguiente() != null)
           {
               temp = temp.getSiguiente();
           }
           NodoDE nuevo = new NodoDE(dato); 
           temp.setSiguiente(nuevo);
           nuevo.setAnterior(temp);
           
       } else {
           cabeza = new NodoDE(dato);
       }
    }
    
    public void adicionarNodoDEAlInicio(Infante dato) 
    {
       if(cabeza != null)
       {
           NodoDE nuevo = new NodoDE(dato);
           nuevo.setSiguiente(cabeza);
           cabeza.setAnterior(nuevo);
           cabeza = nuevo;
       } else {
           cabeza = new NodoDE(dato);
       }
    }
    
    public void invertirLista() 
    {
        NodoDE temp = cabeza;
        listaDoblementeEnlazada listaCopia = new listaDoblementeEnlazada();
        while(temp != null)
        {
           listaCopia.adicionarNodoDEAlInicio(temp.getDato());
           temp = temp.getSiguiente();
        }
        cabeza = listaCopia.getCabeza();
    }
    
    
    public int contarNodoDE() 
    {
        if(cabeza != null)
        {
           int cont = 0;
           NodoDE temp = cabeza;
           while(temp != null)
           {
               cont++;
               temp = temp.getSiguiente();
           }
           return cont;
        } else {
            return 0;
        }
    }
    
//    public Infante buscarNodo(int posicion) 
//    {
//        
//        return Infante;
//    }
    
    public boolean eliminarNodoDE(Infante datoEliminar)
    {
        if(cabeza!=null)
        {
            if(cabeza.getDato().getNombre().equals(datoEliminar.getNombre()))
            {
                cabeza=cabeza.getSiguiente();
                if (cabeza != null) {
                    cabeza.setAnterior(null);
                }
                return true;
            }
            else
            {
                NodoDE temp=cabeza;
                while(temp!=null)
                {
                    if(temp.getSiguiente()!=null)
                    {
                        if(temp.getSiguiente().getDato().getNombre().equals(datoEliminar.getNombre()))
                        {
                            NodoDE nodo = temp.getSiguiente().getSiguiente();
                            temp.setSiguiente(nodo);
                            nodo.setAnterior(temp);
                            
                            return true;
                        }
                    }
                    temp=temp.getSiguiente();
                }
                
            }
        }
        
        return false;
    }
    
    public boolean eliminarNodoDEPosicion(int pos) {
        if (cabeza != null) {
            if(pos == 1)
            {
                cabeza= cabeza.getSiguiente();
                if (cabeza != null) {
                    cabeza.setAnterior(null);
                }
                return true;
            } else {
                NodoDE temp = cabeza;
                int contador = 1;
                if (pos > 0 && pos <= contarNodoDE()) {
                    while (temp != null) {
                        if(temp.getSiguiente()!=null)
                        {
                            if(contador+1 == pos)
                            {
                                NodoDE nodo = temp.getSiguiente().getSiguiente();
                                if(nodo != null) {
                                    temp.setSiguiente(nodo);
                                    nodo.setAnterior(temp);
                                } else {
                                    temp.setSiguiente(nodo);
                                }
 
                                return true;
                            }
                        }
                        temp=temp.getSiguiente();
                        contador++;
                    }
                }
                return false;
            }
        }
        return false;
    }
    
    public String listarNodoDE() 
    {
        if(cabeza != null)
        {
           String listado = "";
           NodoDE temp = cabeza;
           while(temp != null)
           {
               listado = listado+temp.getDato();
               temp = temp.getSiguiente();
           }
           return listado;
        } else {
            return "Hola Mundo";
        }
        
    }
    
    public List<Infante> listarNodoDEInfante() 
    {
        if(cabeza != null)
        {
           List<Infante> infantes = new ArrayList();
           NodoDE temp = cabeza;
           while(temp != null)
           {
               infantes.add(temp.getDato());
               temp = temp.getSiguiente();
           }
           return infantes;
        } else {
            return null;
        }
        
    }
    
    public void modificarListaMenores() 
    {
        NodoDE temp = cabeza;
        listaDoblementeEnlazada listaCopia = new listaDoblementeEnlazada();
        while(temp!=null)
        {
            if(temp.getDato().getEdad() < 3) 
            {
                listaCopia.adicionarNodoDEAlInicio(temp.getDato());
                temp = temp.getSiguiente();
            }
            else
            {
                listaCopia.adicionarNodoDE(temp.getDato());
                temp = temp.getSiguiente();
            }
        }
        cabeza = listaCopia.getCabeza();
    }
    
    public NodoDE irAlUltimo()
    {
      NodoDE temp = cabeza;
      if(cabeza != null)
      {
        while(temp.getSiguiente() != null) {                
            temp = temp.getSiguiente();
        }
        return temp;
      }
        return null;
    }
}
