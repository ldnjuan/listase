/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Comercial
 */
public class listaSimplementeEnlazada {
    private Nodo cabeza;

    
    public Nodo getCabeza() {
        return cabeza;
    }

    public void setCabeza(Nodo cabeza) {
        this.cabeza = cabeza;
    }

    @Override
    public String toString() {
        return "listaSimplementeEnlazada{" + "cabeza=" + cabeza + '}';
    }
    
    
    
    public void adicionarNodo(Infante dato) 
    {
       if(cabeza != null)
       {
           Nodo temp = cabeza;
           while(temp.getSiguiente() != null)
           {
               temp = temp.getSiguiente();
           }
           temp.setSiguiente(new Nodo(dato));
       } else {
           cabeza = new Nodo(dato);
       }
    }
    
    public void adicionarNodoAlInicio(Infante dato) 
    {
       if(cabeza != null)
       {
           Nodo nuevo = new Nodo(dato);
           nuevo.setSiguiente(cabeza);
           cabeza = nuevo;
       } else {
           cabeza = new Nodo(dato);
       }
    }
    
    public void invertirLista() 
    {
        Nodo temp = cabeza;
        listaSimplementeEnlazada listaCopia = new listaSimplementeEnlazada();
        while(temp != null)
        {
           listaCopia.adicionarNodoAlInicio(temp.getDato());
           temp = temp.getSiguiente();
        }
        cabeza = listaCopia.getCabeza();
    }
    
    
    public int contarNodo() 
    {
        if(cabeza != null)
        {
           int cont = 0;
           Nodo temp = cabeza;
           while(temp != null)
           {
               cont++;
               temp = temp.getSiguiente();
           }
           return cont;
        } else {
            return 0;
        }
    }
    
//    public Infante buscarNodo(int posicion) 
//    {
//        
//        return Infante;
//    }
    
    public boolean eliminarNodo(Infante datoEliminar)
    {
            if(cabeza!=null)
            {
                if(cabeza.getDato().getNombre().equals(datoEliminar.getNombre()))
                {
                    cabeza= cabeza.getSiguiente();
                    return true;
                }
                else
                {
                    Nodo temp=cabeza;
                    while(temp!=null)
                    {
                        if(temp.getSiguiente()!=null)
                        {
                            if(temp.getSiguiente().getDato().getNombre().equals(datoEliminar.getNombre()))
                            {
                                temp.setSiguiente(temp.getSiguiente().getSiguiente());
                                return true;
                            }
                        }
                        temp=temp.getSiguiente();
                    }

                }
                return false;
            }
        return false;
    }
    
    public boolean eliminarNodoPosicion(int pos) {
        if (cabeza != null) {
            if(pos == 1)
            {
                cabeza= cabeza.getSiguiente();
                return true;
            } else {
                Nodo temp = cabeza;
                int contador = 1;
                if (pos > 0 && pos <= contarNodo()) {
                    while (temp != null) {
                        if(temp.getSiguiente()!=null)
                        {
                            if(contador+1 == pos)
                            {
                                temp.setSiguiente(temp.getSiguiente().getSiguiente());
                                return true;
                            }
                        }
                        temp=temp.getSiguiente();
                        contador++;
                    }
                }
                return false;
            }
        }
        return false;
    }
    
    public String listarNodo() 
    {
        if(cabeza != null)
        {
           String listado = "";
           Nodo temp = cabeza;
           while(temp != null)
           {
               listado = listado+temp.getDato();
               temp = temp.getSiguiente();
           }
           return listado;
        } else {
            return "Hola Mundo";
        }
        
    }
    
    public List<Infante> listarNodoInfante() 
    {
        if(cabeza != null)
        {
           List<Infante> infantes = new ArrayList();
           Nodo temp = cabeza;
           while(temp != null)
           {
               infantes.add(temp.getDato());
               temp = temp.getSiguiente();
           }
           return infantes;
        } else {
            return null;
        }
        
    }
    
    public void modificarListaMenores() 
    {
        Nodo temp = cabeza;
        listaSimplementeEnlazada listaCopia = new listaSimplementeEnlazada();
        while(temp!=null)
        {
            if(temp.getDato().getEdad() < 3) 
            {
                listaCopia.adicionarNodoAlInicio(temp.getDato());
                temp = temp.getSiguiente();
            }
            else
            {
                listaCopia.adicionarNodo(temp.getDato());
                temp = temp.getSiguiente();
            }
        }
        cabeza = listaCopia.getCabeza();
    }
    
    public Nodo irAlUltimo()
    {
      Nodo temp = cabeza;
      if(cabeza != null)
      {
        while(temp.getSiguiente() != null) {                
            temp = temp.getSiguiente();
        }
        return temp;
      }
        return null;
    }
    
}
